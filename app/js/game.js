///<reference path="../../bower_components/phaser/typescript/phaser.d.ts"/>
///<reference path="./Actor.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var GAME_WIDTH = 600;
var GAME_HEIGHT = 800;
var Game;
(function (Game) {
    var Actors = (function () {
        function Actors() {
        }
        return Actors;
    })();
    Game.Actors = Actors;
    var Objects;
    (function (Objects) {
        var Rock = (function (_super) {
            __extends(Rock, _super);
            function Rock(phaser) {
                _super.call(this, phaser);
                this.sprite = phaser.add.sprite(0, 0, 'rock');
                this.bounce_fx = phaser.add.audio("rock-fx");
                console.log(this.bounce_fx);
                this.bounce_fx.play();
                this.reset();
            }
            Rock.prototype.update = function () {
                if (this.sprite.position.y > GAME_HEIGHT) {
                    this.bounce.stop();
                    this.reset();
                }
            };
            Rock.prototype.reset = function () {
                this.sprite.position.y = Rock.POSITION_Y_SPAWN;
                this.sprite.position.x = Math.random() * GAME_WIDTH;
                this.doBounce();
            };
            Rock.prototype.doBounce = function () {
                var _this = this;
                this.bounce = this.phaser.add.tween(this.sprite);
                this.bounce.onComplete.add(function () {
                    _this.onBounceComplete(_this);
                });
                var dist_to_bounce = (Math.random() * Rock.MAX_DIST_BETWEEN_BOUNCE_Y);
                var new_y = this.sprite.position.y + Rock.MIN_BOUNCE_DIST_Y + dist_to_bounce;
                var tweak_ratio = dist_to_bounce / Rock.MAX_DIST_BETWEEN_BOUNCE_Y;
                var new_x = Math.round((this.sprite.position.x + (50 - (Math.random() * 100))));
                this.bounce.to({ x: new_x, y: new_y }, Rock.MIN_BOUNCE_TIME + ((Math.random() * Rock.MAX_BOUNCE_LENGTH_DIFFERENCE) * tweak_ratio), Phaser.Easing.Bounce.Out);
                this.bounce.start();
            };
            Rock.prototype.onBounceComplete = function (rock) {
                rock.bounce_fx.play();
                rock.doBounce();
            };
            Rock.preload = function (phaser) {
                phaser.load.image("rock", "app/images/rock-sm.png");
                phaser.load.audio("rock-fx", "app/audio/rock.wav");
            };
            Rock.POSITION_Y_SPAWN = 0;
            Rock.MAX_ASTEROIDS = 10;
            Rock.MIN_BOUNCE_TIME = 400;
            Rock.MIN_BOUNCE_DIST_Y = 80;
            Rock.MAX_DIST_BETWEEN_BOUNCE_Y = 100;
            Rock.MAX_BOUNCE_LENGTH_DIFFERENCE = 200;
            return Rock;
        })(Game.Actor);
        Objects.Rock = Rock;
        /**
         *
         */
        var Player = (function (_super) {
            __extends(Player, _super);
            function Player(phaser) {
                _super.call(this, phaser);
                this.sprite = this.phaser.add.sprite(Player.PLAYER_X_OFFSET_START, Player.PLAYER_Y_OFFSET, "player");
                this.sprite.animations.add("walk", [0, 1, 2, 3, 4, 5, 6, 7, 8], 15, true);
                this.sprite.play("walk");
            }
            Player.prototype.update = function () {
                //                if ( game.cursor.left.isDown ) {
                //                    this.sprite.position.x -= Player.PLAYER_SPEED;
                //                }
                //                else if ( game.cursor.right.isDown ) {
                //                    this.sprite.position.x += Player.PLAYER_SPEED;
                //                }
                this.sprite.position.clampX(Player.PLAYER_X_MIN, Player.PLAYER_X_MAX);
            };
            Player.preload = function (phaser) {
                phaser.load.spritesheet("player", "app/images/player/player_armor.png", Player.PLAYER_WIDTH, Player.PLAYER_HEIGHT);
            };
            Player.PLAYER_WIDTH = 64;
            Player.PLAYER_HEIGHT = 55;
            Player.PLAYER_X_OFFSET_START = (GAME_WIDTH / 2) - (Player.PLAYER_WIDTH / 2);
            Player.PLAYER_Y_OFFSET = 650;
            Player.PLAYER_X_MAX = GAME_WIDTH - (Player.PLAYER_WIDTH / 2);
            Player.PLAYER_X_MIN = Player.PLAYER_WIDTH / 2;
            Player.PLAYER_SPEED = 5;
            return Player;
        })(Game.Actor);
        Objects.Player = Player;
        /**
         *
         */
        var Background = (function (_super) {
            __extends(Background, _super);
            function Background(phaser) {
                _super.call(this, phaser);
            }
            Background.prototype.update = function () {
            };
            Background.preload = function (phaser) {
            };
            return Background;
        })(Game.Actor);
        Objects.Background = Background;
    })(Objects = Game.Objects || (Game.Objects = {}));
    /**
     * Game state; score, etc.
     */
    var State = (function () {
        function State() {
            this._score = 0;
            // public
            this.getScore = function () {
                return this._score;
            };
            this.adjustScore = function (adj_value) {
                this._score += adj_value;
            };
            this.reset();
        }
        State.prototype.reset = function () {
            this._score = 0;
        };
        return State;
    })();
    /***********************************************************
     * Main guts
     * @constructor
     */
    var Main = (function () {
        function Main() {
            var _this = this;
            this.cursor = null;
            this.state = new State();
            this.actors = new Game.Actors();
            this.update = function (game) {
                game.actors.player.update();
                for (var i in game.actors.rocks) {
                    game.actors.rocks[i].update();
                }
                //game.state.textScore.text = "Score: " + this.state.getScore();
                game.state.adjustScore(1);
            };
            this.phaser = new Phaser.Game(GAME_WIDTH, GAME_HEIGHT, Phaser.AUTO, '', {
                preload: function () {
                    _this.preload(_this);
                },
                create: function () {
                    _this.create(_this);
                },
                update: function () {
                    _this.update(_this);
                }
            });
        }
        Main.prototype.preload = function (game) {
            Game.Objects.Background.preload(game.phaser);
            Game.Objects.Player.preload(game.phaser);
            Game.Objects.Rock.preload(game.phaser);
        };
        Main.prototype.create = function (game) {
            game.phaser.physics.startSystem(Phaser.Physics.P2JS);
            game.cursor = game.phaser.input.keyboard.createCursorKeys();
            game.actors.background = new Game.Objects.Background(game.phaser);
            game.actors.player = new Game.Objects.Player(game.phaser);
            game.actors.rocks = [];
            for (var i = 0; i < Objects.Rock.MAX_ASTEROIDS; i++) {
                game.actors.rocks[i] = new Game.Objects.Rock(game.phaser);
            }
            //        _this.state.textScore = _this.phaser.add.text( 15, 10,
            //                                                       "Score:", {font: "30px Arial", fill: "#fff", align: "left"} );
        };
        return Main;
    })();
    Game.Main = Main;
})(Game || (Game = {}));
var game = new Game.Main();
//# sourceMappingURL=game.js.map