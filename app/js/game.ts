///<reference path="../../bower_components/phaser/typescript/phaser.d.ts"/>
///<reference path="./Actor.ts"/>

const GAME_WIDTH = 600;
const GAME_HEIGHT = 800;

module Game
{
    export class Actors
    {
        background : Actor;
        player : Actor;
        rocks : Actor[];
    }

    export module Objects
    {
        export class Rock extends Actor
        {
            static POSITION_Y_SPAWN = 0;
            static MAX_ASTEROIDS = 10;
            //static POSITION_Y_OUTOFBOUNDS = Player.PLAYER_Y_OFFSET + 100;

            bounce : Phaser.Tween;
            bounce_fx : Phaser.Sound;

            constructor( phaser : Phaser.Game )
            {
                super( phaser );
                this.sprite = phaser.add.sprite( 0, 0, 'rock' );
                this.bounce_fx = phaser.add.audio( "rock-fx" );
                console.log( this.bounce_fx );
                this.bounce_fx.play();
                this.reset();
            }

            public update()
            {

                if ( this.sprite.position.y > GAME_HEIGHT ) {
                    this.bounce.stop();
                    this.reset();
                }
            }

            private reset()
            {
                this.sprite.position.y = Rock.POSITION_Y_SPAWN;
                this.sprite.position.x = Math.random() * GAME_WIDTH;
                this.doBounce();
            }

            static MIN_BOUNCE_TIME = 400;
            static MIN_BOUNCE_DIST_Y = 80;
            static MAX_DIST_BETWEEN_BOUNCE_Y = 100;
            static MAX_BOUNCE_LENGTH_DIFFERENCE = 200;

            doBounce()
            {
                this.bounce = this.phaser.add.tween( this.sprite );
                this.bounce.onComplete.add( ()=>
                                            {
                                                this.onBounceComplete( this );
                                            } );

                var dist_to_bounce = (Math.random() * Rock.MAX_DIST_BETWEEN_BOUNCE_Y);
                var new_y = this.sprite.position.y + Rock.MIN_BOUNCE_DIST_Y + dist_to_bounce;
                var tweak_ratio = dist_to_bounce / Rock.MAX_DIST_BETWEEN_BOUNCE_Y;

                var new_x = Math.round( (this.sprite.position.x + (50 - (Math.random() * 100))) );

                this.bounce.to(
                        {x: new_x, y: new_y},
                        Rock.MIN_BOUNCE_TIME + ((Math.random() * Rock.MAX_BOUNCE_LENGTH_DIFFERENCE) * tweak_ratio),
                        Phaser.Easing.Bounce.Out
                );

                this.bounce.start();
            }

            onBounceComplete( rock )
            {
                rock.bounce_fx.play();
                rock.doBounce();
            }

            static preload( phaser : Phaser.Game )
            {
                phaser.load.image( "rock", "app/images/rock-sm.png" );
                phaser.load.audio( "rock-fx", "app/audio/rock.wav" );
            }
        }

        /**
         *
         */
        export class Player extends Actor
        {
            static PLAYER_WIDTH = 64;
            static PLAYER_HEIGHT = 55;

            static PLAYER_X_OFFSET_START = (GAME_WIDTH / 2) - (Player.PLAYER_WIDTH / 2);
            static PLAYER_Y_OFFSET = 650;

            static PLAYER_X_MAX = GAME_WIDTH - (Player.PLAYER_WIDTH / 2);
            static PLAYER_X_MIN = Player.PLAYER_WIDTH / 2;

            static PLAYER_SPEED = 5;

            constructor( phaser : Phaser.Game )
            {
                super( phaser );
                this.sprite = this.phaser.add.sprite( Player.PLAYER_X_OFFSET_START, Player.PLAYER_Y_OFFSET, "player" );
                this.sprite.animations.add( "walk", [0, 1, 2, 3, 4, 5, 6, 7, 8], 15, true );
                this.sprite.play( "walk" );
            }

            update()
            {
//                if ( game.cursor.left.isDown ) {
//                    this.sprite.position.x -= Player.PLAYER_SPEED;
//                }
//                else if ( game.cursor.right.isDown ) {
//                    this.sprite.position.x += Player.PLAYER_SPEED;
//                }

                this.sprite.position.clampX( Player.PLAYER_X_MIN, Player.PLAYER_X_MAX );
            }

            static preload( phaser : Phaser.Game )
            {
                phaser.load.spritesheet( "player", "app/images/player/player_armor.png", Player.PLAYER_WIDTH, Player.PLAYER_HEIGHT );
            }
        }

        /**
         *
         */
        export class Background extends Actor
        {

            constructor( phaser : Phaser.Game )
            {
                super( phaser );
            }

            update()
            {

            }

            static preload( phaser : Phaser.Game )
            {
            }
        }

    }

    /**
     * Game state; score, etc.
     */
    class State
    {
        _score : number = 0;

        private reset()
        {
            this._score = 0;
        }

        // public
        getScore = function ()
        {
            return this._score;
        }

        adjustScore = function ( adj_value )
        {
            this._score += adj_value;
        }

        constructor()
        {
            this.reset();
        }
    }

    /***********************************************************
     * Main guts
     * @constructor
     */
    export class Main
    {
        phaser : Phaser.Game;
        cursor : Phaser.CursorKeys = null;

        state = new State();
        actors = new Game.Actors();

        constructor()
        {
            this.phaser = new Phaser.Game( GAME_WIDTH, GAME_HEIGHT, Phaser.AUTO, '',
                                           {
                                               preload: ()=>
                                               {
                                                   this.preload( this );
                                               },
                                               create:  ()=>
                                               {
                                                   this.create( this );
                                               },
                                               update:  ()=>
                                               {
                                                   this.update( this );
                                               }
                                           } );
        }

        preload( game : Game.Main )
        {
            Game.Objects.Background.preload( game.phaser );
            Game.Objects.Player.preload( game.phaser );
            Game.Objects.Rock.preload( game.phaser );
        }

        create( game : Game.Main )
        {
            game.phaser.physics.startSystem( Phaser.Physics.P2JS );
            game.cursor = game.phaser.input.keyboard.createCursorKeys();

            game.actors.background = new Game.Objects.Background( game.phaser );

            game.actors.player = new Game.Objects.Player( game.phaser );

            game.actors.rocks = [];
            for ( var i = 0; i < Objects.Rock.MAX_ASTEROIDS; i++ ) {
                game.actors.rocks[i] = new Game.Objects.Rock( game.phaser );
            }

//        _this.state.textScore = _this.phaser.add.text( 15, 10,
//                                                       "Score:", {font: "30px Arial", fill: "#fff", align: "left"} );
        }

        update = function ( game : Game.Main )
        {
            game.actors.player.update();
            for ( var i in game.actors.rocks ) {
                game.actors.rocks[i].update();
            }

            //game.state.textScore.text = "Score: " + this.state.getScore();

            game.state.adjustScore( 1 );
        };
    }
}

var game = new Game.Main();