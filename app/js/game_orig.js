"use strict";

const GAME_WIDTH = 600;
const GAME_HEIGHT = 800;

const PLAYER_WIDTH = 64;
const PLAYER_HEIGHT = 55;

const PLAYER_X_OFFSET_START = (GAME_WIDTH / 2) - (PLAYER_WIDTH / 2);
const PLAYER_Y_OFFSET = 650;

const PLAYER_X_MAX = GAME_WIDTH - (PLAYER_WIDTH / 2);
const PLAYER_X_MIN = PLAYER_WIDTH / 2;

const PLAYER_SPEED = 5;

const MAX_ASTEROIDS = 10;

// namespace
var Game = Game || {};

// space for our active objects
Game.Objects = {
    player: {},
    rocks:  []
};

/**
 * Enemy rock definition
 * @param {Game} game
 * @constructor
 */
Game.Objects.Rock = function ( game ) {
    const POSITION_Y_SPAWN = 0;
    const POSITION_Y_OUTOFBOUNDS = PLAYER_Y_OFFSET + 100;

    var game = game;
    var sprite;
    var bounce;
    var bounce_fx;

//    var in_bounce = false;

    this.getSprite = function () {
        return sprite;
    }

    function reset() {
        sprite.position.y = POSITION_Y_SPAWN;
        sprite.position.x = Math.random() * GAME_WIDTH;
        //       in_bounce = false;
        doBounce();
    }

    const MIN_BOUNCE_TIME = 400;
    const MIN_BOUNCE_DIST_Y = 80;
    const MAX_DIST_BETWEEN_BOUNCE_Y = 100;
    const MAX_BOUNCE_LENGTH_DIFFERENCE = 200;

    function doBounce() {
        bounce = game.phaser.add.tween( sprite );
        bounce.onComplete.add( onBounceComplete );

        var dist_to_bounce = (Math.random() * MAX_DIST_BETWEEN_BOUNCE_Y);
        var new_y = sprite.position.y + MIN_BOUNCE_DIST_Y + dist_to_bounce;
        var tweak_ratio = dist_to_bounce / MAX_DIST_BETWEEN_BOUNCE_Y;

        var new_x = Math.round( (sprite.position.x + (50 - (Math.random() * 100))) );

        bounce.to(
                {x: new_x, y: new_y},
                MIN_BOUNCE_TIME + ((Math.random() * MAX_BOUNCE_LENGTH_DIFFERENCE) * tweak_ratio),
                Phaser.Easing.Bounce.Out
        );

        bounce.start();
        //in_bounce = true;
    }

    function onBounceComplete() {
        bounce_fx.play();
        doBounce();
    }

    this.update = function () {

        if ( sprite.position.y > GAME_HEIGHT ) {
            bounce.stop();
            reset();
        }
    }

    // construct
    sprite = game.phaser.add.sprite( 0, 0, 'rock' );
    bounce_fx = game.phaser.add.audio( "rock-fx" );
    reset();
}

Game.Objects.Rock.preload = function ( phaser ) {
    phaser.load.image( "rock", "app/images/rock-sm.png" );
    phaser.load.audio( "rock-fx", "app/audio/rock.wav" );
}

/**
 *
 * @param game
 * @constructor
 */
Game.Objects.Background = function ( game ) {

    var game = game;
    var sprite;
    var bounce;
    var bounce_fx;

//    var in_bounce = false;

    this.getSprite = function () {
        return sprite;
    }

    function reset() {
    }

    this.update = function () {
    }

    // construct
    sprite = game.phaser.add.sprite( 0, 0, 'rock' );
    bounce_fx = game.phaser.add.audio( "rock-fx" );
    reset();
}

Game.Objects.Background.preload = function ( phaser ) {
//    phaser.load.image( "rock", "app/images/rock-sm.png" );
//    phaser.load.audio( "rock-fx", "app/audio/rock.wav" );
}


/***********************************************************
 *
 * @param {Game} game
 * @constructor
 */
Game.Objects.Player = function ( game ) {
    var game = game;
    var sprite;

    this.update = function () {
        if ( game.cursor.left.isDown ) {
            this.sprite.position.x -= PLAYER_SPEED;
        }
        else if ( game.cursor.right.isDown ) {
            this.sprite.position.x += PLAYER_SPEED;
        }

        this.sprite.position.clampX( PLAYER_X_MIN, PLAYER_X_MAX );
    }

    this.sprite = game.phaser.add.sprite( PLAYER_X_OFFSET_START, PLAYER_Y_OFFSET, "player" );
    this.sprite.animations.add( "walk", [0, 1, 2, 3, 4, 5, 6, 7, 8], 15, true );
    this.sprite.play( "walk" );
}

Game.Objects.Player.preload = function ( phaser ) {
    phaser.load.spritesheet( "player", "app/images/player/player_armor.png", PLAYER_WIDTH, PLAYER_HEIGHT );
}

/***********************************************************
 * Game state; score, etc.
 * @constructor
 */
Game.State = function () {
    var _score = 0;

    // private
    function reset() {
        _score = 0;
    }

    // public
    this.getScore = function () {
        return _score;
    }

    this.adjustScore = function ( adj_value ) {
        _score += adj_value;
    }

    reset();
}

/***********************************************************
 * Main guts
 * @constructor
 */
Game.Main = function () {
    var _this = this;

    this.phaser = null;
    this.state = new Game.State();
    this.cursor = null;

    /**
     *
     */
    this.preload = function () {
        console.log( "preload" );
        Game.Objects.Player.preload( this );
        Game.Objects.Rock.preload( this );
    };

    /**
     *
     */
    this.create = function () {
        console.log( "create" );
        this.physics.startSystem( Phaser.Physics.P2JS );
        _this.cursor = this.input.keyboard.createCursorKeys();

        Game.Objects.player = new Game.Objects.Player( _this );

        var k = 0;

        for ( var i = 0; i < MAX_ASTEROIDS; i++ ) {
            Game.Objects.rocks[i] = new Game.Objects.Rock( _this );
        }

        _this.state.textScore = _this.phaser.add.text( 15, 10,
                                                       "Score:", {font: "30px Arial", fill: "#fff", align: "left"} );
    };

    /**
     *
     */
    this.update = function () {
        Game.Objects.player.update();
        for ( var i in Game.Objects.rocks ) {
            Game.Objects.rocks[i].update();
        }

        _this.state.textScore.text = "Score: " + _this.state.getScore();

        _this.state.adjustScore( 1 );
    };

    /**
     * construct
     * @type {Phaser.Game}
     */
    this.phaser = new Phaser.Game( GAME_WIDTH, GAME_HEIGHT, Phaser.AUTO, '',
                                   {
                                       preload: this.preload,
                                       create:  this.create,
                                       update:  this.update
                                   }
    );
}

var game = new Game.Main();